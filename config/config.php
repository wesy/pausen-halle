<?php
/**
 * Created by PhpStorm.
 * User: Sebastian Kotte
 * Date: 09.12.2018
 * Time: 12:15
 */

$file = 'config.csv';

$file_handle = fopen($file, 'r');

echo "var config = [";

$config = "";

/**
 * As long as not end of file
 */
while (!feof($file_handle)) {
    //Current line
    $line = fgets($file_handle);
    //Split at the semicolon
    $array = explode(";", $line);
    //Ignore first line
    if (!startsWith($line, "﻿Image name") && strlen($line) > 0) {
        //Generate js code
        $config .= "[";
        for ($i = 0; $i < sizeof($array); $i++) {
            if ($i == sizeof($array) - 1) {
                $config .= "\"" . substr($array[$i], 0, -2) . "\"";
            } else {
                $config .= "\"" . $array[$i] . "\", ";
            }
        }
        $config .= "],";
    }
}
//Remove last space;
$config = substr($config, 0, -1);
$config .= "];";
fclose($file_handle);

echo $config;

/**
 * @param $haystack
 * @param $needle
 * @return bool
 */
function startsWith($haystack, $needle)
{
    return $needle === ''
        || strrpos($haystack, $needle, -strlen($haystack)) !== false;
}