<?php
/**
 * Created by PhpStorm.
 * User: Patrick Wilke
 * Date: 12.03.2019
 * Time: 23:37
 */

include "utils/fileReader.php";
include "utils/fileWriter.php";

//deletes a slide

$return = fileReader("../config/config.csv");

$lines = explode('$', $return);
$counter = 0;
for ($i = 0; $i < sizeof($lines); $i++) {
    if (explode(';', $lines[$i])[0] != $_GET["slide"]) {
        $config[$counter] = $lines[$i]; //deletes the config of this file
        $counter++;
    }
}

writeInConfig($config, "config");
if ($_GET["slide"] != "livestream") {

    $unlinkSlide = "../slideshow/" . $_GET["slide"];    //deletes the file
    unlink($unlinkSlide);
}
header("Location: index.php");