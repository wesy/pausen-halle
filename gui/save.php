<!DOCTYPE html>
<html lang="de">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.2/css/all.css">
    <link rel="stylesheet" type="text/css" href="stylesheet/stylesheet.css">
    <link id="theme" rel="stylesheet"/>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="../js/CookieHelper.js"></script>
    <script src="../js/Settings.js"></script>
    <title>Slideshow</title>
</head>


<body onload="updatePrefs();">

<div class="topnav">
    <a href="index.php">Gallery</a>
    <a href="defaultConfig.php">Default Config</a>
    <a href="displayGroup.php">Display Groups</a>

    <button type="button" class="btn btn-default btn-sm dropdown-toggle" id="settingsButton" data-toggle="dropdown">
        <i class="fa fa-cog fa-lg" aria-hidden="true"></i>
    </button>
    <ul class="dropdown-menu" id="settingsDropdown">
        <li>
            <div class="custom-control custom-checkbox" id="checkBoxHolderTV">
                <input type="checkbox" class="custom-control-input" id="tableViewToggle" onclick="toggleTableView()">
                <label class="custom-control-label" for="tableViewToggle">Table View</label>
            </div>
        </li>
        <li>
            <div class="custom-control custom-checkbox" id="checkBoxHolderTS">
                <input type="checkbox" class="custom-control-input" id="themeToggle" onclick="switchTheme()">
                <label class="custom-control-label" for="themeToggle">Dark Theme</label>
            </div>
        </li>
        <div class="dropdown-divider"></div>
        <li>
            <a class="dropdown-item" href="../crash-logs/">Crash logs</a>
        </li>
    </ul>
</div>

<?php
echo "<h1>Please Wait</h1><br>";
echo "<h1>Config Will be Saved</h1>";
include "utils/fileReader.php";
include "utils/fileWriter.php";
if ($_GET["file"] == "config") {

    //stores entered data in the config of the slide
    $return = fileReader("../config/config.csv");
    $config = $_GET["text0"] . ";";
    $startTimeInSeconds = strtotime($_POST["startTime"]) - strtotime('TODAY');
    $endTimeInSeconds = strtotime($_POST["endTime"]) - strtotime('TODAY');

    if ($_POST["durationUnit"] != "infinity") {
        if ($_POST["durationNumber"] == null || $_POST["durationUnit"] == "") {
            $config .= "" . ";";
        } else {
            $config .= $_POST["durationNumber"];
            $config .= $_POST["durationUnit"] . ";";
        }


        if ($_POST["intervalNumber"] == null || $_POST["intervalUnit"] == "") {
            $config .= "" . ";";
        } else {
            $config .= $_POST["intervalNumber"];
            $config .= $_POST["intervalUnit"] . ";";
        }

        if ($_POST["startTime"] != null && $_POST["endTime"] != null && $startTimeInSeconds < $endTimeInSeconds) {
            $startHours = intval(explode(":", $_POST["startTime"])[0]);
            $endHours = intval(explode(":", $_POST["endTime"])[0]);

            $startMinutes = intval(explode(":", $_POST["startTime"])[1]);
            $endMinutes = intval(explode(":", $_POST["endTime"])[1]);

            $timeDifference = ($endHours - $startHours) * 60 + $endMinutes - $startMinutes;
            if ($timeDifference >= 0) {
                $config .= $timeDifference . "m;";
            } else {
                $config .= " ;";
            }

            $config .= $_POST["startTime"] . ";";
        } else {
            $config .= ";;";
        }
    } else {
        $config .= "infinity;;;;";
    }
    $lines = explode("$", $return);
    for ($i = 0; $i < sizeof($lines) - 1; $i++) {
        $temp = (explode(";", $lines[$i]));
        if ($temp[0] == $_GET["text0"]) {
            $lines[$i] = $config . "\n";
        }
    }

    writeInConfig($lines, $_GET["file"]);

} else if ($_GET["file"] == "default") {
    $return = fileReader("../config/slide_show_config.csv");
    $lines = explode('$', $return);


    $lines[0] = "Default delay;";
    if ($_POST["delayNumber"] == null || $_POST["delayUnit"] == "") {
        $lines[0] .= "" . ";\n";
    } else {
        $lines[0] .= $_POST["delayNumber"];
        $lines[0] .= $_POST["delayUnit"] . ";\n";
    }

    $lines[1] = "Default image;" . $_POST["imageValue"] . ";\n";
    $lines[2] = "Default stream;" . $_POST["streamValue"] . ";\n";
    $lines[3] = "time_offset;" . $_POST["timeOffsetNumber"] . ";\n";

    $lines[4] = "refresh_rate;";
    if ($_POST["refreshRateNumber"] == null || $_POST["refreshRateUnit"] == "") {
        $lines[4] .= "" . ";\n";
    } else {
        $lines[4] .= $_POST["refreshRateNumber"];
        $lines[4] .= $_POST["refreshRateUnit"] . ";\n";
    }

    writeInConfig($lines, $_GET["file"]);
}

header("Location: index.php");
?>
</body>
</html>
