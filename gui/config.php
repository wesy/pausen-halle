<!DOCTYPE html>
<html lang="de">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.2/css/all.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="stylesheet/stylesheet.css">
    <link id="theme" rel="stylesheet"/>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="../js/CookieHelper.js"></script>
    <script src="../js/Settings.js"></script>
    <title>Edit File Config</title>
</head>


<body onload="updatePrefs();">

<div class="topnav">
    <div class="dropdown-container" id="mobileNavigatorContainer">
        <button type="button" class="btn btn-default btn-sm dropdown-toggle" id="mobileNavigator"
                data-toggle="dropdown">
            <i class="fa fa-bars fa-2x" aria-hidden="true"></i>
        </button>
        <ul class="dropdown-menu" id="mobileNavigatorDropdown">
            <li>
                <a class="dropdown-item" href="index.php">Gallery</a>
            </li>
            <li>
                <a class="dropdown-item" href="defaultConfig.php">Default Config</a>
            </li>
            <li>
                <a class="dropdown-item" href="displayGroup.php">Display Groups</a>
            </li>
        </ul>
    </div>
    <a href="index.php">Gallery</a>
    <a href="defaultConfig.php">Default Config</a>
    <a href="displayGroup.php">Display Groups</a>

    <?php
    include "utils/fileReader.php";
    include "utils/fileWriter.php";

    $return = fileReader("../config/config.csv");
    $name = $_GET["name"];
    $lines = explode('$', $return);
    $config = array();
    for ($i = 0; $i < sizeof($lines); $i++) {
        if (explode(";", $lines[$i])[0] == $name) {
            $config = explode(";", $lines[$i]);
        }
    }

    echo "  <form action='deleteSlide.php?slide=" . $name . "' method='post'>";
    echo "        <input class='btn btn-danger' id='deleteConfig' type='submit' value='Delete'>";
    echo "    </form>";
    echo "    <form action='save.php?text0=" . $config[0] . "&file=config' method='post'>";
    echo "        <input class='btn btn-success' id='saveConfig' type='submit' value='Save'>";

    echo "<div class=dropdown-container>";
    echo "<button type='button' class='btn btn-default btn-sm dropdown-toggle' id='settingsButton' data-toggle='dropdown'>";
    echo "        <i class='fa fa-cog fa-lg' aria-hidden='true'></i>";
    echo "    </button>";
    echo "    <ul class='dropdown-menu' id='settingsDropdown'>";
    echo "        <li>";
    echo "            <div class='custom-control custom-checkbox' id='checkBoxHolderTV'>";
    echo "                <input type='checkbox' class='custom-control-input' id='tableViewToggle' onclick='toggleTableView()'>";
    echo "                <label class='custom-control-label' for='tableViewToggle'>Table View</label>";
    echo "            </div>";
    echo "        </li>";
    echo "        <li>";
    echo "            <div class='custom-control custom-checkbox' id='checkBoxHolderTS'>";
    echo "                <input type='checkbox' class='custom-control-input' id='themeToggle' onclick='switchTheme()'>";
    echo "                <label class='custom-control-label' for='themeToggle'>Dark Theme</label>";
    echo "            </div>";
    echo "        </li>";
    echo "<div class='dropdown-divider'></div>";
    echo "        <li>";
    echo "            <a class='dropdown-item' href='../crash-logs/'>Crash logs</a>";
    echo "        </li>";
    echo "    </ul>";
    echo "</div>";
    echo "</div>";


    // Table
    echo "<table class='table table-striped'>";
    // Image name
    echo "<tr id='configFirstRow'>";
    echo "    <td>Image name</td>";
    echo "    <td>" . $name . "</td>";
    echo "</tr>";

    // Display duration
    $durationNumberValue = "";
    for ($i = 0; $i < sizeof(str_split($config[1])); $i++) {
        $durationNumberValue .= str_split($config[1])[$i];
    }
    echo "<tr>";
    echo "    <td>Display duration</td>";
    echo "    <td>";
    echo "    <div class='input-group'>";
    echo "    <input name='durationNumber' class='form-control' type='number' value= '" . (strlen($durationNumberValue) > 0 ? intval($durationNumberValue) : null) . "' min='0'>";
    echo "    <div class='input-group-prepend'>";
    echo "    </div>";
    echo "        <select onchange='selectInfinityOption()' name='durationUnit' class='form-control dropdown-selector'>";
    echo "            <option></option>";?>

    <option <?php ((str_split($config[1])[sizeof(str_split($config[1])) - 1] == 's') ? $select=" selected='selected'" : $select="");?><?php echo $select?> value='s'>seconds</option>
    <option <?php ((str_split($config[1])[sizeof(str_split($config[1])) - 1] == 'm') ? $select=" selected='selected'" : $select="");?> <?php echo $select?> value='m'>minutes</option>
    <option <?php ((str_split($config[1])[sizeof(str_split($config[1])) - 1] == 'h') ? $select=" selected='selected'" : $select="");?> <?php echo $select?> value='h'>hours</option>
    <?php
    // only add infinity option when url contains "livestream"
    $url = $_SERVER['REQUEST_URI'];
    if (strpos($url,'livestream') !== false) {
        ((str_split($config[1])[sizeof(str_split($config[1])) - 1] == 'y') ? $select=" selected='selected'" : $select="");
        echo "<option " . $select . " id='infinityOption' value='infinity'>infinity</option>";
    }
    ?>

    <?php
    echo "              </select>";
    echo "    </td>";
    echo "</tr>";

    //Interval
    $intervalNumberValue = "";
    for ($i = 0; $i < sizeof(str_split($config[2])); $i++) {
        $intervalNumberValue .= str_split($config[2])[$i];
    }
    echo "<tr>";
    echo "    <td>Interval</td>";
    echo "    <td>";
    echo "    <div class='input-group'>";
    echo "    <input name='intervalNumber' class='form-control' type='number' value='" . (strlen($intervalNumberValue) > 0 ? intval($intervalNumberValue) : null) . "' min='0'>";
    echo "    <div class='input-group-prepend'>";
    echo "    </div>";
    echo "        <select name='intervalUnit' class='form-control dropdown-selector'>
                    <option></option>
                    " . ((str_split($config[2])[sizeof(str_split($config[2])) - 1] == 's') ? "<option selected=selected value='s'>seconds</option>" : "<option value='s'>seconds</option>") . "
                    " . ((str_split($config[2])[sizeof(str_split($config[2])) - 1] == 'm') ? "<option selected=selected value='m'>minutes</option>" : "<option value='m'>minutes</option>") . "
                    " . ((str_split($config[2])[sizeof(str_split($config[2])) - 1] == 'h') ? "<option selected=selected value='h'>hours</option>" : "<option value='h'>hours</option>") . "
                  </select>";
    echo "    </td>";
    echo "</tr>";

    //Start Time
    $minuteDifference = "";
    for ($i = 0; $i < strlen($config[3]) - 1; $i++) {
        $minuteDifference .= str_split($config[3])[$i];
    }

    if (strlen($config[4]) > 0) {

        $endTimeString = $config[4];

        $startTime = date("H:i", strtotime($endTimeString) - $minuteDifference * 60);
        $startTimeHours = (string)date("H", strtotime($startTime));
        $startTimeMinutes = (string)date("i", strtotime($startTime));

    } else {
        $startTimeHours = "--";
        $startTimeMinutes = "--";
    }

    echo "<tr>";
    echo "<td>Time</td>";
    echo "<td>";
    echo "<div class='input-group'>";
    echo "<span class=\"input-group-text\">From</span>";
    echo "<input name='startTime' class='form-control' type='time' value='" . $startTimeHours . ":" . $startTimeMinutes . "'>";
    echo "<div class='input-group-prepend'>";
    echo "<span class=\"input-group-text\" style=\"border-left:0;border-right:0;\">To</span>";
    echo "</div>";

    //End Time
    $timeValues = "";
    for ($i = 4; $i < sizeof($config); $i++) {
        if (sizeof($config) - 1 > $i + 1) {
            $timeValues .= $config[$i] . ";";
        } else {
            $timeValues .= $config[$i];
        }
    }
    echo "<input name='endTime' class='form-control' type='time' value='" . (strlen($config[4]) > 0 ? $config[4] : null) . "'></div></td>";
    echo "</tr>";

    echo "<br>";
    echo "<div class='preview'>";
    $slideFormat = explode(".", $name)[sizeof(explode(".", $name)) - 1];
    if ($slideFormat == "mp4" || $slideFormat == "flv" || $slideFormat == "avi" || $slideFormat == "mov" || $slideFormat == "WebM") {
        echo "    <video src='" . "../slideshow/" . $name . "#t=2" . "' alt='" . $name . "' muted loop>";
    } else if (explode(")", $name)[sizeof(explode(")", $name)) - 1] == "livestream") {
        echo "    <img class=\"images\" src='../assets/LiveStream.png' alt=\"" . $name . "\">";
    } else {
        echo "<img src='" . "../slideshow/" . $name . "' alt='" . $name . "'>";
    }
    echo "</div>";
    echo "<br></form>";
    ?>
    <div class="alert" id="infinityWarning">
        <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
        <strong>Warning!</strong> Only this slide will be displayed.
    </div>

    <script>
        selectInfinityOption();

        function selectInfinityOption() {

        	let forms = document.getElementsByClassName("form-control");
			let warning = document.getElementById("infinityWarning");

			if (document.getElementById("infinityOption") === null)
				warning.style.display = "none";

            if (document.getElementById("infinityOption").selected !== true) {
				for (let i = 0; i < forms.length; i++) {
					forms[i].disabled = false;
				}
                warning.style.display = "none";

            } else {

				for (let i = 0; i < forms.length; i++) {
					if (i === 1)
						i++;
					forms[i].value = null;
					forms[i].disabled = true;
				}
				warning.style.display = "";
            }
        }

        let videos = document.querySelectorAll("video");
        videos.forEach(video => {
            video.addEventListener("mouseover", function () {
                this.play();
            });

            video.addEventListener("mouseout", function () {
                this.pause();
            });

            video.addEventListener("touchstart", function () {
                this.play();
            });

            video.addEventListener("touchend", function () {
                this.pause();
            });
        });


    </script>

</body>
</html>