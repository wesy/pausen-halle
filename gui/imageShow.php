<?php
echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"stylesheet/stylesheet.css\">";
echo "<link id=\"theme\" rel=\"stylesheet\"/>";
include "utils/fileReader.php";
include "utils/FileSize.php";


//Function for displaying the slides
function imageShow()
{
    $imageNames = getSlideNames("../config/config.csv");

    for ($i = 0; $i < sizeof($imageNames) - 1; $i++) {
        $temp = "../slideshow/" . $imageNames[$i];

        if ($imageNames[$i] != 'Image name') {
            echo "<a href=\"config.php?name=" . $imageNames[$i] . "\"><div class=gallery >";
            echo "    <div class='imgOverlay'>" . $imageNames[$i] . "</div>";
            echo "    <form action='deleteSlide.php?slide=" . $imageNames[$i] . "' method='post'>";
            echo "        <input class='btn btn-danger' id='deleteGalleryElement' type='submit' value='Delete'>";
            echo "    </form>";
            $slideFormat = explode(".", $imageNames[$i])[sizeof(explode(".", $imageNames[$i])) - 1];
            if ($slideFormat == "mp4" || $slideFormat == "flv" || $slideFormat == "avi" || $slideFormat == "mov" || $slideFormat == "WebM") {
                echo "    <video src='" . $temp . "#t=2" . "' alt='" . $imageNames[$i] . "' preload='metadata' muted loop>";
            } else if (explode(")", $imageNames[$i])[sizeof(explode(")", $imageNames[$i])) - 1] == "livestream") {
                echo "    <img class=\"images\" src='../assets/LiveStream.png' alt=\"" . $imageNames[$i] . "\">";
            } else {
                echo "    <img class=\"images\" src='" . $temp . "' alt=\"" . $imageNames[$i] . "\">";
            }
            echo "    <div class=\"desc\">" . $imageNames[$i] . "</div>";
            echo "    </div>";
            echo "</a>";
        }
    }
}

//Function for displaying the slides in a table
function imageShowTable()
{
    $imageNames = getSlideNames("../config/config.csv");
    echo "<table class='table table-striped' id='slideTable'>";

    for ($i = 0; $i < sizeof($imageNames) - 1; $i++) {
        if ($imageNames[$i] != 'Image name') {
            echo "<tr>";
            echo "    <td id='galleryElementName'>" . $imageNames[$i] . "</td>";
            echo "    <td id='galleryElementFileSize'>" . getFileSize($imageNames[$i], '../slideshow/') . "</td>";
            echo "    <td id='galleryElementEdit'>";
            echo "        <form action=\"config.php?name=" . $imageNames[$i] . "\" method='post'>";
            echo "            <input class='btn btn-primary btn-lg' type='submit' value='Edit'>";
            echo "        </form>";
            echo "    </td>";
            echo "    <td id='galleryElementDelete'>";
            echo "        <form action='deleteSlide.php?slide=" . $imageNames[$i] . "' method='post'>";
            echo "            <input class='btn btn-danger btn-lg' type='submit' value='Delete'>";
            echo "        </form>";
            echo "    </td>";
            echo "</tr>";
        }
    }
}