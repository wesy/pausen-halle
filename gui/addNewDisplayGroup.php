<!DOCTYPE html>
<html lang="de">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.2/css/all.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="stylesheet/stylesheet.css">
    <link id="theme" rel="stylesheet"/>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="../js/DisplayGroupHelper.js"></script>
    <script src="../js/CookieHelper.js"></script>
    <script src="../js/Settings.js"></script>
    <title>Edit Display Group</title>
</head>



<body onload="updatePrefs();">
<div class="topnav">
    <div class="dropdown-container" id="mobileNavigatorContainer">
        <button type="button" class="btn btn-default btn-sm dropdown-toggle" id="mobileNavigator"
                data-toggle="dropdown">
            <i class="fa fa-bars fa-2x" aria-hidden="true"></i>
        </button>
        <ul class="dropdown-menu" id="mobileNavigatorDropdown">
            <li>
                <a class="dropdown-item" href="index.php">Gallery</a>
            </li>
            <li>
                <a class="dropdown-item" href="defaultConfig.php">Default Config</a>
            </li>
            <li>
                <a class="dropdown-item active" href="displayGroup.php">Display Groups</a>
            </li>
        </ul>
    </div>
    <a href="index.php">Gallery</a>
    <a href="defaultConfig.php">Default Config</a>
    <a class="active" href="displayGroup.php">Display Groups</a>
    <form action='displayGroup.php'>
        <button class='btn btn-danger' id='goBack' type='submit'>Back</button>
    </form>

    <?php
    include "utils/fileReader.php";
    include "utils/ReadDisplayGroupData.php";
    include "utils/FileSize.php";
    echo "<form name='groupForm' method='post' id='groupForm'>";
    echo "<input class='btn btn-success' id='saveConfig' type='button' value='Save' onclick='save()'>";

    echo "<div class=dropdown-container>";
    echo "<button type='button' class='btn btn-default btn-sm dropdown-toggle' id='settingsButton' data-toggle='dropdown'>";
    echo "        <i class='fa fa-cog fa-lg' aria-hidden='true'></i>";
    echo "    </button>";
    echo "    <ul class='dropdown-menu' id='settingsDropdown'>";
    echo "        <li>";
    echo "            <div class='custom-control custom-checkbox' id='checkBoxHolderTV'>";
    echo "                <input type='checkbox' class='custom-control-input' id='tableViewToggle' onclick='toggleTableView()'>";
    echo "                <label class='custom-control-label' for='tableViewToggle'>Table View</label>";
    echo "            </div>";
    echo "        </li>";
    echo "        <li>";
    echo "            <div class='custom-control custom-checkbox' id='checkBoxHolderTS'>";
    echo "                <input type='checkbox' class='custom-control-input' id='themeToggle' onclick='switchTheme()'>";
    echo "                <label class='custom-control-label' for='themeToggle'>Dark Theme</label>";
    echo "            </div>";
    echo "        </li>";
    echo "<div class='dropdown-divider'></div>";
    echo "        <li>";
    echo "            <a class='dropdown-item' href='../crash-logs/'>Crash logs</a>";
    echo "        </li>";
    echo "    </ul>";
    echo "</div>";
    echo "</div>";


    $imageNames = getSlideNames("../config/config.csv");
    if (isset($_POST["fileName"])) {
        $json = getDisplayGroupData("../display_groups/" . $_POST["fileName"]);
    }
    echo "<table class='table table-striped'>";
    echo "<tr><td>Name</td><td><input type='text' name='displayGroupName' class='form-control' value='" . (isset($_POST["fileName"]) ? $json["displayName"] : null) . "'/></td></tr>";
    echo "<tr><td>IP Address</td>";
    echo "<td>";
    echo "    <input name='ipAddress' required id='ipAddress' type='text' class='form-control' value='" . (isset($_POST["fileName"]) ? $json["localIPAddress"] : null) . "'/>";
    echo "</td>";
    echo "</tr>";
    echo "<tr><td>Livestream-URL</td>";
    echo "<td><input type = 'text' disabled id='urlInput' onkeyup='updateSlideUrl()' class='form-control'/></td></tr>";
    echo "<tr><td>Slides</td><td><select multiple=\"multiple\" class=\"form-control\" name='selectedSlides[]' id='selectedSlides'>";

    for ($i = 0; $i < sizeof($imageNames); $i++) {
        $isSelected = false;
        if (strlen($imageNames[$i]) < 2) {
            continue;
        }
        for ($j = 0; $j < sizeof($json["slides"]); $j++) {
            if ($imageNames[$i] == $json["slides"][$j]["name"]) {
                $isSelected = true;
                break;
            }
        }

        if ($isSelected) {
            echo "<option data-url='".(isset($_POST["fileName"]) ? $json["slides"][$j]["url"] : null)."' data-animin='" . (isset($_POST["fileName"]) ? $json["slides"][$j]["fadeIn"] : null) . "' data-animout='" . (isset($_POST["fileName"]) ? $json["slides"][$j]["fadeOut"] : null) . "' class='slide' selected value = '" . $imageNames[$i] . "'>" . $imageNames[$i] . " " . getFileSize($imageNames[$i], "../slideshow/", true) . "</option>";
        } else {
            echo "<option data-url='' data-animin='fadeInLeft' data-animout='fadeOutRight' class='slide' value = '" . $imageNames[$i] . "'>" . $imageNames[$i] . " " . getFileSize($imageNames[$i], "../slideshow/", true) . "</option>";
        }

    }
    echo "</select></td></tr>";
    echo "</table>";

    $animations = explode("$", fileReader("../config/animation.anim"));
    $fadeInAnimations = Array();
    $fadeOutAnimations = Array();
    for ($i = 0; $i < sizeof($animations) - 1; $i++) {
        $fadeInAnimations[$i] = explode(";", $animations[$i])[0];
        $fadeOutAnimations[$i] = explode(";", $animations[$i])[1];
    }



    echo "<table class='table table-striped'>";
    echo "<tr><td class='animHeader'>Slide-In Animation</td><td class='animHeader'>Slide-Out Animation</td><td id='previewHeader'>Preview:</td></tr>";
    echo "<tr>";
    echo "<td class='slideSelectorCell'><select class='form-control dropdown-selector' disabled onchange='updateSlideAnimations()'>";
    for ($i = 0; $i < sizeof($fadeInAnimations); $i++) {
        echo "<option>" . $fadeInAnimations[$i] . "</option>";
    }
    echo "</select></td>";
    echo "<td class='slideSelectorCell'><select class='form-control dropdown-selector' disabled onchange='updateSlideAnimations()'>";
    for ($i = 0; $i < sizeof($fadeOutAnimations); $i++) {
        echo "<option>" . $fadeOutAnimations[$i] . "</option>";
    }
    echo "</select></td>";
    echo "<td>";
    echo "    <iframe id='previewFrame' class='w-100 h-100 border-0 horizontal' src='animPreview.php'></iframe>";
    echo "</td>";
    echo "</tr>";
    echo "</table>";
    echo "</form>";

    // a way to pass POST values to javascript
    if (isset($_POST["fileName"])) {
        echo "    <input type='hidden' value = '" . $_POST["fileName"] . "'  id='isEdit'>";
    }

    ?>

    <script>

        document.getElementById('previewFrame').onload = function () {
            let preview = window.frames[0].document.getElementById("preview");
            preview.className = "";
            preview.classList.add("animated", "fadeIn", "infinite");
            preview.addEventListener("animationiteration", function () {
                switchAnimation();
            });
        };

        function save() {
            let ip = document.getElementById('ipAddress');

            // https://regex101.com/r/lcqSke
            let regexp = new RegExp("^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$");

            // checks whether the entered ip address is valid
            // if it is -> save
            if (regexp.test(ip.value)) {

                let selectedSlides = Array();
                for (let i = 0; i < document.getElementById("groupForm")["selectedSlides"].selectedOptions.length; i++) {
                    let fadeInAnim = document.getElementById("groupForm")["selectedSlides"].selectedOptions[i].dataset.animin;
                    let fadeOutAnim = document.getElementById("groupForm")["selectedSlides"].selectedOptions[i].dataset.animout;
                    let url = document.getElementById("groupForm")["selectedSlides"].selectedOptions[i].dataset.url;
                    let slide = {
                        "name": document.getElementById("groupForm")["selectedSlides"].selectedOptions[i].value,
                        "fadeIn": fadeInAnim,
                        "fadeOut": fadeOutAnim,
                        "url": url
                    };
                    selectedSlides.push(slide);
                }

                let ipAddress = document.getElementById("ipAddress").value;

                if (document.getElementById("isEdit") !== null && document.getElementById("isEdit").value.split(".json")[0] !== ipAddress) {
                    // when editing a file: deletes old file before creating a new one
                    deleteDisplayGroup(document.getElementById("isEdit").value);
                }

                saveDisplayGroup(document.getElementById("groupForm")["displayGroupName"].value, ipAddress, selectedSlides);

                setTimeout(function () {
                    window.location = 'displayGroup.php';
                }, 25); // we need to wait a few ms before going back to displayGroup.php
            }
            // if not -> display error
            else {
                ip.setCustomValidity("Please enter a valid IP Address");
                ip.reportValidity();
            }

        }

        let currentSlide;

        function updateSlideAnimations() {
            let animIn = document.getElementsByClassName("dropdown-selector")[0].value;
            let animOut = document.getElementsByClassName("dropdown-selector")[1].value;
            currentSlide[0].dataset.animin = animIn;
            currentSlide[0].dataset.animout = animOut;
        }

        // override default mousedown event for option elements
        // gets last recently selected option and changes the preview image accordingly
        // fills the animation dropdowns with the correct animations
        // disables or enables the animation selectors & preview depending on whether the last clicked option is selected or not
        $('.slide').mousedown(function (e) {
            let selectors = document.getElementsByClassName("dropdown-selector");
            let preview = window.frames[0].document.getElementById("preview");
            e.preventDefault();
            $(this).prop('selected', !$(this).prop('selected'));
            if ($(this).prop('selected')) {

                selectors[0].disabled = false;
                selectors[1].disabled = false;
                preview.style.visibility = "visible";

                document.getElementById("urlInput").disabled = !this.value.includes("livestream");

                currentSlide = $(this);
                selectors[0].value = currentSlide[0].dataset.animin;
                selectors[1].value = currentSlide[0].dataset.animout;
                document.getElementById("urlInput").value = currentSlide[0].dataset.url;

                document.getElementById("previewHeader").innerText = "Preview: " + this.value;


                if (this.value.includes(".jpeg") || this.value.includes(".jpg") || this.value.includes(".png") || this.value.includes(".gif") || this.value.includes(".svg"))
                    preview.src = "../slideshow/" + this.value;
                else
                    preview.src = "../assets/Preview.png";
            }
            else {
                document.getElementById("previewHeader").innerText = "Preview: ";
                preview.style.visibility = "hidden";
                selectors[0].value = "fadeIn";
                selectors[1].value = "fadeOut";
                selectors[0].disabled = true;
                selectors[1].disabled = true;
                document.getElementById("urlInput").disabled = true;
            }
        });

        function updateSlideUrl(){
            currentSlide[0].dataset.url = document.getElementById("urlInput").value;
        }


        // switches between fade-In and fade-Out animations after the current animation has ended
        function switchAnimation() {
            let preview = window.frames[0].document.getElementById("preview");
            let fadeInAnim = document.getElementsByClassName("dropdown-selector")[0].value;
            let fadeOutAnim = document.getElementsByClassName("dropdown-selector")[1].value;

            if (preview.className.includes(fadeInAnim)) {
                preview.className = "";
                preview.classList.add("animated", fadeOutAnim, "infinite");
            } else {
                preview.className = "";
                preview.classList.add("animated", fadeInAnim, "infinite");
            }
        }

    </script>

</body>
</html>
