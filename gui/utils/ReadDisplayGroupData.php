<?php
function getDisplayGroupData($path){
    $jsonString = file_get_contents($path);
    $displayGroup = json_decode($jsonString, true);
    return $displayGroup;
}