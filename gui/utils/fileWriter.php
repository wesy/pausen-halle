<?php
/**
 * Created by PhpStorm.
 * User: Patrick Wilke
 * Date: 07.02.2019
 * Time: 17:11
 */

//writes the config to a file
function writeInConfig($config, $file){
    if($file == 'config'){
        $file = fopen("../config/config.csv", "w");
    }else if($file == 'default'){
        $file = fopen("../config/slide_show_config.csv", "w");
    }

    for($i = 0; $i < sizeof($config); $i++){
        fwrite($file, $config[$i]);
    }
    fclose($file);
}