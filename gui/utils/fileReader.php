<?php
/**
 * Created by PhpStorm.
 * User: Patrick Wilke
 * Date: 02.02.2019
 * Time: 16:15
 */

//read the file and return it in the format row$row...
function fileReader($path){

    if (!is_file($path)) {
        $file = fopen($path, 'w');    // Save our content to the file.
        fclose($file);
    }


    $file = fopen($path, "r");
    $return ="";
    while($contains = fgets($file, 4096)){
        if (strlen($contains) > 2) {
            $return .= $contains."$";
        }
    }
    fclose($file);
    return $return;
}

function getSlideNames($path){
    $slides = fileReader($path);
    $fileLines = explode('$', $slides); //splits the scanned file back into their lines
    $imageNames = array();

    for($i = 0; $i < sizeof($fileLines); $i++){
        $imageNames[$i] = explode(';', $fileLines[$i])[0];  //contains the names of the slides
    }
    return $imageNames;
}