<?php
/**
 * Created by PhpStorm.
 * User: Patrick Chabowski
 * Date: 10.05.2019
 * Time: 19:52
 * @param $file
 * @param null $filePath
 * @param bool $brackets
 * @return string
 */

function getFileSize($file, $filePath = NULL, $brackets = false) {

    if (!file_exists($filePath . $file)) {

        if($brackets)
            return "(N/A)";
        else
            return "N/A";
    }
    else {
        $fileSize = intval(filesize($filePath . $file));
    }

    if ($fileSize > 1024 * 1024 * 1024) {
        if ($brackets)
            return "(" . number_format((float)$fileSize / (1024 * 1024 * 1024), 0, '.', '') . "GB)";
        else
            return number_format((float)$fileSize / (1024 * 1024 * 1024), 0, '.', '') . "GB";
    }
    else if ($fileSize > 1024 * 1024) {
        if ($brackets)
            return "(" . number_format((float)$fileSize / (1024 * 1024), 0, '.', '') . "MB)";
        else
            return number_format((float)$fileSize / (1024 * 1024), 0, '.', '') . "MB";
    }
    else if ($fileSize > 1024) {
        if ($brackets)
            return " (" . number_format((float)$fileSize / 1024, 0, '.', '') . "KB)";
        else
            return number_format((float)$fileSize / 1024, 0, '.', '') . "KB";
    }
    else if ($fileSize < 1024) {
        if ($brackets)
            return "(" . number_format((float)$fileSize, 0, '.', '') . "B)";
        else
            return number_format((float)$fileSize, 0, '.', '') . "B";
    }
    else {
        return "";
    }
}