<?php
/**
 * Created by PhpStorm.
 * User: Sebastian Kotte
 * Date: 26.04.2019
 * Time: 17:09
 *
 */

$localIP = $_GET['LocalIP'];
$directory = '../../display_groups/';

$filePath = search_file($directory, $localIP . ".json");
if (filesize($filePath) > 0) {
    $json = json_decode(file_get_contents($filePath), true);
    foreach ($json as $key => $values) {
        if ($key == 'slides') {
            echo 'var displayGroupSlides = ' . json_encode($values) . ";";
        }
    }
} else {
    echo 'var displayGroupSlides = [];';
}

function search_file($dir, $file_to_search)
{
    $files = scandir($dir);
    foreach ($files as $key => $value) {
        $path = realpath($dir . DIRECTORY_SEPARATOR . $value);
        if (!is_dir($path)) {
            if ($file_to_search == $value) {
                return $path;
            }
        } else if ($value != "." && $value != "..") {
            search_file($path, $file_to_search);
        }
    }
    return null;
}