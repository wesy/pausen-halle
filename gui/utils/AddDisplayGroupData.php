<?php
/**
 * Created by PhpStorm.
 * User: Sebastian Kotte
 * Date: 25.04.2019
 * Time: 13:39
 *
 */

$jsonString = $_POST['jsonString'];
$fileName = $_POST['fileName'];

if (!file_exists("../../display_groups/")) {
    mkdir("../../display_groups/");
}

$myfile = fopen('../../display_groups/' . $fileName . ".json", "w") or die("Unable to open file!");
$txt = $jsonString;
fwrite($myfile, $txt);
fclose($myfile);