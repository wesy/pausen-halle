<!DOCTYPE html>
<html lang="de">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link rel="stylesheet" type="text/css" href="stylesheet/stylesheet.css"/>
    <link id="theme" rel="stylesheet"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.2/css/all.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="../js/CookieHelper.js"></script>
    <script src="../js/Settings.js"></script>
    <title>Gallery</title>
</head>


<body onload="updatePrefs();">

<div class="topnav">
    <div class="dropdown-container" id="mobileNavigatorContainer">
        <button type="button" class="btn btn-default btn-sm dropdown-toggle" id="mobileNavigator"
                data-toggle="dropdown">
            <i class="fa fa-bars fa-2x" aria-hidden="true"></i>
        </button>
        <ul class="dropdown-menu" id="mobileNavigatorDropdown">
            <li>
                <a class="dropdown-item active" href="index.php">Gallery</a>
            </li>
            <li>
                <a class="dropdown-item" href="defaultConfig.php">Default Config</a>
            </li>
            <li>
                <a class="dropdown-item" href="displayGroup.php">Display Groups</a>
            </li>
            <div class="dropdown-divider mobile"></div>
            <li>
                <div class="search mobile">
                    <input type="text" class="searchBar mobile" id="searchDisplayGroupsMobile" onkeyup="search('searchDisplayGroupsMobile')"
                           placeholder="Search...">
                    <button type="submit" class="searchButton mobile">
                        <i class="fa fa-search"></i>
                    </button>
                </div>
            </li>
        </ul>
    </div>
    <a class="active" href="index.php">Gallery</a>
    <a href="defaultConfig.php">Default Config</a>
    <a href="displayGroup.php">Display Groups</a>
    <form action='new.html' method='post'>
        <input class="btn btn-success" id="addElement" type="submit" value="Add">
    </form>

    <div class="dropdown-container">
        <button type="button" class="btn btn-default btn-sm dropdown-toggle" id="settingsButton" data-toggle="dropdown">
            <i class="fa fa-cog fa-lg" aria-hidden="true"></i>
        </button>
        <ul class="dropdown-menu" id="settingsDropdown">
            <li>
                <div class="custom-control custom-checkbox" id="checkBoxHolderTV">
                    <input type="checkbox" class="custom-control-input" id="tableViewToggle"
                           onclick="toggleTableView()">
                    <label class="custom-control-label" for="tableViewToggle">Table View</label>
                </div>
            </li>
            <li>
                <div class="custom-control custom-checkbox" id="checkBoxHolderTS">
                    <input type="checkbox" class="custom-control-input" id="themeToggle" onclick="switchTheme()">
                    <label class="custom-control-label" for="themeToggle">Dark Theme</label>
                </div>
            </li>
            <div class="dropdown-divider"></div>
            <li>
                <a class="dropdown-item" href="../crash-logs/">Crash logs</a>
            </li>
        </ul>
    </div>


    <?php
    include "imageShow.php";

    $cookie = 'tableView';
    if (isset($_COOKIE[$cookie])) {
        if ($_COOKIE[$cookie] == 'true') {
            echo "    <div class='search'>";
            echo "        <input type='text' class='searchBar' id='searchDisplayGroups' onkeyup='search(\"searchDisplayGroups\")' placeholder='Search...'>";
            echo "        <button type='submit' class='searchButton'>";
            echo "            <i class='fa fa-search'></i>";
            echo "        </button>";
            echo "    </div>";
            echo "</div>";
            imageShowTable();
        } else {
            echo "</div>";
            imageShow();
        }
    } else {
        echo "</div>";
        imageShow();
    }
    ?>


    <script>
		//only shows groups matching the search term from the search bar
		function search(id) {
			let input, td, txtValue;
			input = document.getElementById(id);

			for (let i = 0; i < document.getElementById("slideTable").getElementsByTagName("tr").length; i++) {
				td = document.getElementById("slideTable").getElementsByTagName("tr")[i].getElementsByTagName("td")[0];
				if (td) {
					txtValue = td.textContent || td.innerText;
					if (txtValue.toUpperCase().indexOf(input.value.toUpperCase()) > -1) {
						document.getElementById("slideTable").getElementsByTagName("tr")[i].style.display = "";
					} else {
						document.getElementById("slideTable").getElementsByTagName("tr")[i].style.display = "none";
					}
				}
			}
		}

		let overlays = document.querySelectorAll(".gallery .imgOverlay");
		overlays.forEach(overlay => {
			let video = overlay.nextElementSibling.nextElementSibling;

			if (video.className !== "images") {
				overlay.addEventListener("mouseover", function () {
					video.play();
				});

				overlay.addEventListener("mouseout", function () {
					video.pause();
				});

				overlay.addEventListener("touchstart", function () {
					video.play();
				});

				overlay.addEventListener("touchend", function () {
					video.pause();
				});
			}
		})
    </script>

</body>
</html>