<!DOCTYPE html>
<html lang="de">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.2/css/all.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="stylesheet/stylesheet.css">
    <link id="theme" rel="stylesheet"/>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="../js/CookieHelper.js"></script>
    <script src="../js/Settings.js"></script>
    <title>Edit Default Config</title>
</head>


<body onload="updatePrefs();">
<form action='save.php?text0=&file=default' method='post'>
    <div class="topnav">
        <div class="dropdown-container" id="mobileNavigatorContainer">
            <button type="button" class="btn btn-default btn-sm dropdown-toggle" id="mobileNavigator"
                    data-toggle="dropdown">
                <i class="fa fa-bars fa-2x" aria-hidden="true"></i>
            </button>
            <ul class="dropdown-menu" id="mobileNavigatorDropdown">
                <li>
                    <a class="dropdown-item" href="index.php">Gallery</a>
                </li>
                <li>
                    <a class="dropdown-item active" href="defaultConfig.php">Default Config</a>
                </li>
                <li>
                    <a class="dropdown-item" href="displayGroup.php">Display Groups</a>
                </li>
            </ul>
        </div>
        <a href="index.php">Gallery</a>
        <a class="active" href="defaultConfig.php">Default Config</a>
        <a href="displayGroup.php">Display Groups</a>
        <input type='submit' value='Save' class='btn btn-success' id='saveDefaultConfig'/>

        <div class="dropdown-container">
            <button type="button" class="btn btn-default btn-sm dropdown-toggle" id="settingsButton"
                    data-toggle="dropdown">
                <i class="fa fa-cog fa-lg" aria-hidden="true"></i>
            </button>
            <ul class="dropdown-menu" id="settingsDropdown">
                <li>
                    <div class="custom-control custom-checkbox" id="checkBoxHolderTV">
                        <input type="checkbox" class="custom-control-input" id="tableViewToggle"
                               onclick="toggleTableView()">
                        <label class="custom-control-label" for="tableViewToggle">Table View</label>
                    </div>
                </li>
                <li>
                    <div class="custom-control custom-checkbox" id="checkBoxHolderTS">
                        <input type="checkbox" class="custom-control-input" id="themeToggle" onclick="switchTheme()">
                        <label class="custom-control-label" for="themeToggle">Dark Theme</label>
                    </div>
                </li>
                <div class="dropdown-divider"></div>
                <li>
                    <a class="dropdown-item" href="../crash-logs/">Crash logs</a>
                </li>
            </ul>
        </div>

    </div>

    <?php
    include "utils/fileReader.php";
    $configData = fileReader("../config/slide_show_config.csv");
    $lines = explode("$", $configData);


    echo "<table class='table table-striped'>";

    echo "<tr>";
    echo "<td class='defaultConfigFirstCell'>﻿﻿Default Delay</td>";
    $tempDelayNumberValue = "";
    for ($i = 0; $i < sizeof(str_split(explode(';', $lines[0])[1])); $i++) {
        $tempDelayNumberValue .= str_split(explode(';', $lines[0])[1])[$i];
    }

    echo "    <td>";
    echo "        <div class='input-group'>";
    echo "        <input name='delayNumber' type='number' class='form-control' value='" . (strlen($tempDelayNumberValue) > 0 ? intval($tempDelayNumberValue) : null) . "' min='0'>";
    echo "        <div class='input-group-prepend'>";
    echo "        </div>";
    echo "        <select name='delayUnit' class='form-control dropdown-selector'>
                    <option></option>
                    " . ((str_split(explode(';', $lines[0])[1])[sizeof(str_split(explode(';', $lines[0])[1])) - 1] == 's') ? "<option selected=selected value='s'>seconds</option>" : "<option value='s'>seconds</option>") . "
                    " . ((str_split(explode(';', $lines[0])[1])[sizeof(str_split(explode(';', $lines[0])[1])) - 1] == 'm') ? "<option selected=selected value='m'>minutes</option>" : "<option value='m'>minutes</option>") . "
                    " . ((str_split(explode(';', $lines[0])[1])[sizeof(str_split(explode(';', $lines[0])[1])) - 1] == 'h') ? "<option selected=selected value='h'>hours</option>" : "<option value='h'>hours</option>") . "
                  </select>";
    echo "    </td>";
    echo "</tr>";
    echo "<tr>";
    echo "    <td class='defaultConfigFirstCell'>Default Image</td>";
    echo "    <td><input class='form-control' type='text' name='imageValue' value=" . explode(';', $lines[1])[1] . "></td>";
    echo "</tr>";

    echo "<tr>";
    echo "    <td class='defaultConfigFirstCell'>Default Stream</td>";
    echo "    <td><input class='form-control' type='text' name='streamValue' value=" . explode(';', $lines[2])[1] . "></td>";
    echo "</tr>";

    echo "<tr>";
    echo "    <td class='defaultConfigFirstCell'>Time Offset</td>";
    echo "    <td><input name='timeOffsetNumber' class='form-control' type='number' value='" . intval(explode(';', $lines[3])[1]) . "' min='0'></td>";
    echo "</tr>";

    echo "<tr>";
    echo "<td class='defaultConfigFirstCell'>Refresh Rate</td>";
    $tempRefreshRateUnit = "";
    for ($i = 0; $i < sizeof(str_split(explode(';', $lines[4])[1])); $i++) {
        $tempRefreshRateUnit .= str_split(explode(';', $lines[4])[1])[$i];
    }

    echo "    <td>";
    echo "    <div class='input-group'>";
    echo "    <input name='refreshRateNumber' type='number' class='form-control' value='" . (strlen($tempRefreshRateUnit) > 0 ? intval($tempRefreshRateUnit) : null) . "' min='0'>";
    echo "    <div class='input-group-prepend'>";
    echo "    </div>";
    echo "    <select name='refreshRateUnit' class='form-control dropdown-selector'>
                <option></option>
                " . ((str_split(explode(';', $lines[4])[1])[sizeof(str_split(explode(';', $lines[4])[1])) - 1] == 's') ? "<option selected=selected value='s'>seconds</option>" : "<option value='s'>seconds</option>") . "
                " . ((str_split(explode(';', $lines[4])[1])[sizeof(str_split(explode(';', $lines[4])[1])) - 1] == 'm') ? "<option selected=selected value='m'>minutes</option>" : "<option value='m'>minutes</option>") . "
                " . ((str_split(explode(';', $lines[4])[1])[sizeof(str_split(explode(';', $lines[4])[1])) - 1] == 'h') ? "<option selected=selected value='h'>hours</option>" : "<option value='h'>hours</option>") . "
              </select>";
    echo "    </td>";
    echo "</tr>";
    echo "<table>";
    ?>

</body>

</html>