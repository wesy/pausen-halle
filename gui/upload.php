<?php
/**
 * Created by PhpStorm.
 * User: Patrick Wilke
 * Date: 12.03.2019
 * Time: 22:33
 */

include "utils/fileReader.php";

//adds one or more slides to the slideshow
$imageNames = getSlideNames("../config/config.csv");
for ($i = 0; $i < sizeof($_FILES['datei']['tmp_name']); $i++) {
    if ($_FILES['datei']['tmp_name'][$i] == null)
        continue;
    $destinationName = $_FILES['datei']['name'][$i];
    $tempName = $destinationName;
    $nameCounter = 1;
    //add a number to the name if that name is already taken
    for ($j = 0; $j < sizeof($imageNames); $j++) {
        while ($imageNames[$j] == $tempName) {
            $tempName = "(" . $nameCounter . ")" . $destinationName;
            $nameCounter++;
            $j=0;
        }
    }

    $destinationName = $tempName;
    move_uploaded_file($_FILES['datei']['tmp_name'][$i], '../slideshow/' . $destinationName);
    $file = fopen("../config/config.csv", "a");
    fwrite($file, "\n" . utf8_encode($destinationName) . ";;;;;");
    fclose($file);
}

header("Location: index.php");