<!DOCTYPE html>
<html lang="de">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="stylesheet/stylesheet.css">
    <link id="theme" rel="stylesheet"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.2/css/all.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="../js/CookieHelper.js"></script>
    <script src="../js/Settings.js"></script>
    <title>Display Groups</title>
</head>


<body onload="updatePrefs();">
<div class="topnav">
    <div class="dropdown-container" id="mobileNavigatorContainer">
        <button type="button" class="btn btn-default btn-sm dropdown-toggle" id="mobileNavigator"
                data-toggle="dropdown">
            <i class="fa fa-bars fa-2x" aria-hidden="true"></i>
        </button>
        <ul class="dropdown-menu" id="mobileNavigatorDropdown">
            <li>
                <a class="dropdown-item" href="index.php">Gallery</a>
            </li>
            <li>
                <a class="dropdown-item" href="defaultConfig.php">Default Config</a>
            </li>
            <li>
                <a class="dropdown-item active" href="displayGroup.php">Display Groups</a>
            </li>
            <div class="dropdown-divider mobile"></div>
            <li>
                <div class="search mobile">
                    <input type="text" class="searchBar mobile" id="searchDisplayGroupsMobile" onkeyup="search('searchDisplayGroupsMobile')"
                           placeholder="Search...">
                    <button type="submit" class="searchButton mobile">
                        <i class="fa fa-search"></i>
                    </button>
                </div>
            </li>
        </ul>
    </div>
    <a href="index.php">Gallery</a>
    <a href="defaultConfig.php">Default Config</a>
    <a class="active" href="displayGroup.php">Display Groups</a>

    <?php
    include "utils/ReadDisplayGroupData.php";
    echo "<form action='addNewDisplayGroup.php' method='post'>";
    echo "    <input class='btn btn-success' id='addElement' type='submit' value='Add'>";
    echo "</form>";

    echo "<div class='dropdown-container'>";
    echo "<button type='button' class='btn btn-default btn-sm dropdown-toggle' id='settingsButton' data-toggle='dropdown'>";
    echo "        <i class='fa fa-cog fa-lg' aria-hidden='true'></i>";
    echo "    </button>";
    echo "    <ul class='dropdown-menu' id='settingsDropdown'>";
    echo "        <li>";
    echo "            <div class='custom-control custom-checkbox' id='checkBoxHolderTV'>";
    echo "                <input type='checkbox' class='custom-control-input' id='tableViewToggle' onclick='toggleTableView()'>";
    echo "                <label class='custom-control-label' for='tableViewToggle'>Table View</label>";
    echo "            </div>";
    echo "        </li>";
    echo "        <li>";
    echo "            <div class='custom-control custom-checkbox' id='checkBoxHolderTS'>";
    echo "                <input type='checkbox' class='custom-control-input' id='themeToggle' onclick='switchTheme()'>";
    echo "                <label class='custom-control-label' for='themeToggle'>Dark Theme</label>";
    echo "            </div>";
    echo "        </li>";
    echo "<div class='dropdown-divider'></div>";
    echo "        <li>";
    echo "            <a class='dropdown-item' href='../crash-logs/'>Crash logs</a>";
    echo "        </li>";
    echo "    </ul>";
    echo "</div>";

    echo "   <div class='search'>";
    echo "      <input type='text' class='searchBar' id='searchDisplayGroups' onkeyup=\"search('searchDisplayGroups')\" placeholder='Search...'>";
    echo "      <button type='submit' class='searchButton'>";
    echo "        <i class='fa fa-search'></i>";
    echo "     </button>";
    echo "   </div>";
    echo "</div>";

    $path = "../display_groups";
    $displayGroups = scandir($path);
    echo "<table class='table table-striped' id='displayGroupTable'>";


    for ($i = 2; $i < sizeof($displayGroups); $i++) {
        echo "<tr>";
        echo "    <td id='displayGroupName'>" . getDisplayGroupData("../display_groups/" . $displayGroups[$i])["displayName"] . "</td>";
        echo "    <td id='displayGroupIpAddress'>" . getDisplayGroupData("../display_groups/" . $displayGroups[$i])["localIPAddress"] . "</td>";
        echo "    <td id='displayGroupEdit'><form action='addNewDisplayGroup.php' method='post'>";
        echo "        <input type='hidden' name='fileName' value='$displayGroups[$i]'>";
        echo "        <input class='btn btn-primary btn-lg' type='submit' value='Edit'>";
        echo "    </form></td>";
        echo "    <td id='displayGroupDelete'><form action='utils/DeleteDisplayGroupData.php' method='post'>";
        echo "        <input type='hidden' name='fileName' value='$displayGroups[$i]'>";
        echo "        <input class='btn btn-danger btn-lg' type='submit' value='Delete'>";
        echo "    </form></td>";
        echo "</tr>";
    }

    ?>

    <script>
        //only shows groups matching the search term from the search bar
		function search(id) {
			let input, name, nameTxtValue, ip, ipTxtValue;
			input = document.getElementById(id);

			for (let i = 0; i < document.getElementById("displayGroupTable").getElementsByTagName("tr").length; i++) {
				name = document.getElementById("displayGroupTable").getElementsByTagName("tr")[i].getElementsByTagName("td")[0];
                ip = document.getElementById("displayGroupTable").getElementsByTagName("tr")[i].getElementsByTagName("td")[1];
				if (name || ip) {
					nameTxtValue = name.textContent || name.innerText;
                    ipTxtValue = ip.textContent || ip.innerText;
					if (nameTxtValue.toUpperCase().indexOf(input.value.toUpperCase()) > -1 || ipTxtValue.toUpperCase().indexOf(input.value.toUpperCase()) > -1) {
						document.getElementById("displayGroupTable").getElementsByTagName("tr")[i].style.display="";
					} else {
						document.getElementById("displayGroupTable").getElementsByTagName("tr")[i].style.display="none";
					}
				}
			}
		}
    </script>


</body>
</html>