<?php
/**
 * Created by PhpStorm.
 * User: Patrick Wilke
 * Date: 20.03.2019
 * Time: 22:13
 */


include "utils/fileReader.php";
//adds a livestream to the slideshow
$imageNames = getSlideNames("../config/config.csv");
$tempName = "livestream";

//add a number to the name if that name is already taken
$nameCounter = 1;
for ($j = 0; $j < sizeof($imageNames); $j++) {

    while ($imageNames[$j] == $tempName) {
        $tempName = "(" . $nameCounter . ")" . "livestream";
        $nameCounter++;
        $j=0;
    }
}

$file = fopen("../config/config.csv", "a");
fwrite($file, "\n" . $tempName . ";;;;;");  //writes a new livestream in config.csv
fclose($file);
header("Location: index.php");