<?php
/**
 * Created by PhpStorm.
 * User: Sebastian Kotte
 * Date: 12.03.2019
 * Time: 17:31
 *
 */

date_default_timezone_set('Europe/Berlin');
$timezone = date_default_timezone_get();
$date = date('Y-m-d H.i.s', time());
$directory = "crash-logs/";
$fileName = "crash-" . $date . ".txt";
$postRequest = $_POST['errorMsg'] . " " . $_POST['script'] . " " . $_POST['line'] . " " . $_POST['column'] . " " . $_POST['error'];

if (!is_dir($directory)) {
    mkdir($directory);
}

$file = fopen($directory . $fileName, "w");
fwrite($file, $postRequest);
fclose($file);