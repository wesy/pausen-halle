/**
 * Created by PhpStorm.
 * User: Patrick Chabowski
 * Date: 05.05.2019
 * Time: 18:57
 */

function toggleTableView() {
	let tableView = document.getElementById("tableViewToggle");

	if (tableView.checked) {
		setCookie("tableView", true, 30);
		tableView.checked = true;
	} else {
		setCookie("tableView", false, 30);
		tableView.checked = false;
	}
	location.reload();
}

function switchTheme() {
	let e = document.getElementById("theme");
	if (e.getAttribute('href') === "stylesheet/dark.css") {
		e.setAttribute('href', "stylesheet/light.css");
		setCookie("darkMode", false, 30);
	} else {
		e.setAttribute('href', "css/dark.css");
		setCookie("darkMode", true, 30);
	}
	location.reload();
}

function updatePrefs() {
	let e = document.getElementById("theme");
	let tableView = document.getElementById("tableViewToggle");
	let tableViewCookie = getCookie("tableView");

	let themeToggle = document.getElementById("themeToggle");
	let darkModeCookie = getCookie("darkMode");

	if (darkModeCookie === "true") {
		e.setAttribute('href', "stylesheet/dark.css");
		themeToggle.checked = true;
	} else {
		e.setAttribute('href', "stylesheet/light.css");
		themeToggle.checked = false;
	}

	tableView.checked = tableViewCookie === "true";
}