loadDisplayGroup();
window.RTCPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection;   //compatibility for firefox and chrome
const pc = new RTCPeerConnection({iceServers: []}), noop = function () {
};
pc.createDataChannel("");    //create a bogus data channel
pc.createOffer(pc.setLocalDescription.bind(pc), noop);    // create offer and set local description
pc.onicecandidate = function (ice) {  //listen for candidate events
	if (!ice || !ice.candidate || !ice.candidate.candidate) return;
	const myIP = /([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/.exec(ice.candidate.candidate)[1];
	pc.onicecandidate = noop;
	setCookie("localIP", myIP, 30);
};

function loadDisplayGroup() {
	let scriptObject = document.createElement('script');
	scriptObject.type = 'text/javascript';
	scriptObject.async = false;
	scriptObject.src = "gui/utils/SlidesForDisplayGroup.php?LocalIP=" + getCookie("localIP");
	document.head.appendChild(scriptObject);
}

if (document.cookie.split('=').length <= 0) {
	location.reload();
} else {
	loadDisplayGroup();
}