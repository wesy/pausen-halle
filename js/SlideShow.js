/**
 * Created by PhpStorm.
 * User: Sebastian Kotte
 * Date: 07.12.2018
 * Time: 20:13
 */

let displayWithoutConfigSlides = [];
let defaultDisplayDurationDelay;
let displayDurationSlides = [];
let allIntervalSlides = [];
let allTimespanSlides = [];
let displayDurationDelay;
let intervalSlides = [];
let timespanSlides = [];
let livestreamStartTime;
let oldLivestreamUrl;
let defaultStream;
let defaultImage;
let timeOffset;
let switchAnimation = false;
let onlyStreaming = false;
let slideIndex = 0;
let video = false;
let time;

/**
 * Updates the slideshow
 */
function slideShow() {
    updateSlide();
    setTimeout(slideShow, displayDurationDelay);
}

function updateSlide() {
    if (timespanSlides.length > 0) {
        nextSlide(timespanSlides);
    } else if (intervalSlides.length > 0) {
        nextSlide(intervalSlides);
    } else if (displayDurationSlides.length > 0) {
        nextSlide(displayDurationSlides);
    } else if (displayWithoutConfigSlides.length > 0) {
        nextSlide(displayWithoutConfigSlides);
    } else {
        document.getElementById("imageSlideShow").src = defaultImage;
        slideShowState(0);
    }
    if (displayDurationSlides.length === 0 && displayWithoutConfigSlides.length === 0) {
        initDisplayDurationSlides();
        initWithoutConfigSlides();
    }
}

/**
 * Loads the next slide
 */
function nextSlide(slideArray) {
    let image = document.getElementById("imageSlideShow");
    let video = document.getElementById("videoSlideShow");
    let imageAnimation = document.getElementById("imageSlideShowAnimation");
    let videoAnimationn = document.getElementById("videoSlideShowAnimation");
    let livestream = document.getElementById("livestream");
    let noConfig = (typeof (slideArray[0]) == "string");
    let filePath = noConfig ? slideArray[0] : slideArray[0][0];
    if (isLivestream(filePath)) {
        livestreamStartTime = document.getElementById('livestream').currentTime;
        for (let displayGroupSlide of displayGroupSlides) {
            if (displayGroupSlide.name === filePath) {
                const hls = new Hls();
                if (displayGroupSlide.url.length === 0) {
                    if (oldLivestreamUrl !== defaultStream) {
                        console.log(oldLivestreamUrl + "\t" + defaultStream);
                        hls.attachMedia(livestream);
                        hls.on(Hls.Events.MEDIA_ATTACHED, function () {
                            hls.loadSource(defaultStream);
                        });
                        oldLivestreamUrl = defaultStream;
                    }
                } else {
                    if (oldLivestreamUrl !== displayGroupSlide.url) {
                        hls.attachMedia(livestream);
                        hls.on(Hls.Events.MEDIA_ATTACHED, function () {
                            hls.loadSource(displayGroupSlide.url);
                        });
                        oldLivestreamUrl = displayGroupSlide.url;
                    }
                }
            }
        }
        if (convertToMilliseconds(slideArray[0][1]) == null) {
            displayDurationDelay = defaultDisplayDurationDelay;
        } else {
            displayDurationDelay = convertToMilliseconds(slideArray[0][1]);
        }
        slideShowState(2);
    } else if (isVideoFormat(filePath)) {
        let bufferVideo;
        let tempVideo = switchAnimation ? video : videoAnimationn;
        filePath = noConfig ? filePath : getSlidePath(filePath);
        bufferVideo = document.getElementById(filePath);
        displayDurationDelay = convertTimeUnitToMilliseconds('s', bufferVideo.duration) + 500;
        tempVideo.src = bufferVideo.src;
        tempVideo.volume = 1;
        tempVideo.currentTime = 0;
        tempVideo.play();
        slideShowState(1);
    } else {
        let switchImage = switchAnimation ? image : imageAnimation;
        switchImage.src = getSlidePath(filePath);
        slideShowState(0);
        displayDurationDelay = (noConfig) ? defaultDisplayDurationDelay : convertToMilliseconds(slideArray[0][1]);
    }
    slideArray.splice(0, 1);
}

/**
 * @param fileName
 * @returns {null} or the file path of the given file name
 */
function getSlidePath(fileName) {
    if (fileName.length > 0) {
        for (let imagePath of imageArray) {
            if (imagePath.endsWith(fileName)) {
                return imagePath;
            }
        }
    }
    return null;
}

/**
 * Fades out the audio if the video ends
 * @deprecated is no longer compatible with animations
 */
function audioFadeOut() {
    setInterval(function () {
        let video = document.getElementById('videoSlideShow');
        let livestream = document.getElementById('livestream');
        if (livestream.style.display === 'block') {
            if (((livestreamStartTime + displayDurationDelay / 1000) - livestream.currentTime) < 5) {
                if (livestream.volume > 0.1) {
                    livestream.volume -= 0.1;
                }
            }
        }
        if (video.style.display === 'block') {
            if (video.duration - video.currentTime < 5) {
                if (video.volume > 0.1) {
                    video.volume -= 0.1;
                }
            }
        }

    }, 500);
}

/**
 * Loads the next video in the Slideshow,
 * to calculate the video duration before it is displayed
 */
function bufferVideos() {
    for (filePath of imageArray) {
        if (isVideoFormat(filePath)) {
            let video = document.createElement('video');
            video.preload = 'metadata';
            video.id = filePath;
            video.src = filePath;
            video.style.display = 'none';
            document.body.appendChild(video);
        }
    }
}

/**
 * Initializes the stream
 */
function initStream() {
    if (Hls.isSupported()) {
        const video = document.getElementById('livestream');
        const hls = new Hls();
        hls.attachMedia(video);
        /** @namespace Hls.Events */
        hls.on(Hls.Events.MEDIA_ATTACHED, function () {
            hls.loadSource(defaultStream);
        });
        hls.on(Hls.Events.ERROR, function (event, data) {
            if (data.fatal) {
                switch (data.type) {
                    case Hls.ErrorTypes.NETWORK_ERROR:
                        console.error("fatal network error encountered, try to recover");
                        break;
                    case Hls.ErrorTypes.MEDIA_ERROR:
                        console.error("fatal media error encountered, try to recover");
                        hls.recoverMediaError();
                        break;
                    default:
                        hls.destroy();
                        break;
                }
                document.getElementById("imageSlideShow").src = defaultImage;
                slideShowState(0);
            }
        });
    }
}


/**
 * Checks if the file a Video
 * @param filePath
 * @returns {boolean}
 */
function isVideoFormat(filePath) {
    let splittedPath = filePath.split(".");
    switch (splittedPath[splittedPath.length - 1]) {
        case "mp4":
            return true;
        case "avi":
            return true;
        case "flv":
            return true;
        case "mov":
            return true;
        case "WebM":
            return true;
    }
    return false;
}

/**
 * Initializes all slides without config data
 */
function initWithoutConfigSlides() {
    for (let imagePath of imageArray) {
        let hasNoConfig = true;
        for (let data of config) {
            if (typeof data !== 'undefined') {
                if (imagePath.endsWith(data[0])) {
                    if (typeof data[1] !== 'undefined') {
                        if (data[1].length > 0 || data[2].length > 0 || data[3].length > 0 || data[4].length > 0) {
                            hasNoConfig = false;
                            break;
                        }
                    }
                }
            }
        }
        if (hasNoConfig) {
            for (let displayGroupSlide of displayGroupSlides) {
                if (displayGroupSlide.name === imagePath.substring(imagePath.lastIndexOf('/') + 1)) {
                    displayWithoutConfigSlides.push(imagePath);
                }
            }
        }
    }
}

function isLivestream(name) {
    return name.endsWith("livestream");
}

/**
 * Initializes all slides that have no interval and no time span
 */
function initDisplayDurationSlides() {
    for (let i = 0; i < config.length; i++) {
        let onlyDisplayDuration = true;
        if (config[i] != null && config[i].length > 1) {
            for (let k = 2; k < config[i].length; k++) {
                if (onlyDisplayDuration) {
                    if (config[i][k].length > 0) {
                        onlyDisplayDuration = false;
                    }
                }
            }
            if (onlyDisplayDuration && ((getSlidePath(config[i][0]) != null) || isLivestream(config[i][0]))) {
                for (let displayGroupSlide of displayGroupSlides) {
                    if (displayGroupSlide.name === config[i][0] && config[i][1].length > 0) {
                        displayDurationSlides.push(config[i]);
                    }
                }
            }
        }
    }
}

/**
 * Initializes all slides with an interval
 */
function initIntervalSlides() {
    for (let i = 0; i < config.length; i++) {
        if (config[i] != null && config[i].length > 1) {
            if (config[i][2].length > 0 && ((getSlidePath(config[i][0]) != null) || isLivestream(config[i][0]))) {
                for (let displayGroupSlide of displayGroupSlides) {
                    if (displayGroupSlide.name === config[i][0]) {
                        allIntervalSlides.push(config[i]);
                    }
                }
            }
        }
    }
}

/**
 * Initializes all slides with time spans
 */
function initTimespanSlides() {
    for (let i = 0; i < config.length; i++) {
        if (config[i] != null && config[i].length > 1) {
            if (config[i][3].length > 0 && config[i][4].length > 0 && config[i][2].length === 0 && ((getSlidePath(config[i][0]) != null) || isLivestream(config[i][0]))) {
                for (let displayGroupSlide of displayGroupSlides) {
                    if (displayGroupSlide.name === config[i][0]) {
                        allTimespanSlides.push(config[i]);
                    }
                }
            }
        }
    }
}

/**
 * Updates every second all slides that have a time span, and checks if they need to be displayed
 */
function updateTimespanSlides() {
    setInterval(function () {
        for (let timespanSlide of allTimespanSlides) {
            if (isInTimespan(timespanSlide)) {
                if (!timespanSlides.includes(timespanSlide)) {
                    timespanSlides.push(timespanSlide);
                }
            }
        }
    }, 1000);
}

/**
 * Checks every second if a slide needs to be displayed with an interval
 */
function updateIntervalSlides(startTime) {
    setInterval(function () {
        for (let intervalSlide of allIntervalSlides) {
            let interval = intervalSlide[2];
            let intTime = parseInt(interval.substr(0, interval.length - 1));
            let charTimeUnit = interval.charAt(interval.length - 1);
            let currentDate = new Date();
            currentDate.setTime(currentDate.getTime() + (timeOffset) * 60 * 60 * 1000);
            switch (interval.charAt(interval.length - 1).toLowerCase()) {
                case 's':
                    if (((currentDate.getTime() - startTime) % convertTimeUnitToMilliseconds(charTimeUnit, intTime)) / 1000 - intTime >= -1) {
                        if (!intervalSlides.includes(intervalSlide)) {
                            if (intervalSlide[4].length > 0 && intervalSlide[3].length > 0) {
                                if (isInTimespan(intervalSlide)) {
                                    intervalSlides.push(intervalSlide);
                                }
                            } else {
                                intervalSlides.push(intervalSlide);
                            }
                        }
                    }
                    break;
                case 'm':
                    if (((currentDate.getTime() - startTime) % convertTimeUnitToMilliseconds(charTimeUnit, intTime)) / (60000) - intTime >= -0.02) {
                        if (!intervalSlide.includes(intervalSlide)) {
                            if (intervalSlide[4].length > 0 && intervalSlide[3].length > 0) {
                                if (isInTimespan(intervalSlide)) {
                                    intervalSlides.push(intervalSlide);
                                }
                            } else {
                                intervalSlides.push(intervalSlide);
                            }
                        }
                    }
                    break;
                case 'h':
                    if (((currentDate.getTime() - startTime) % convertTimeUnitToMilliseconds(charTimeUnit, intTime)) / (3.6e+6) - intTime >= -0.0005) {
                        if (!intervalSlide.includes(intervalSlide)) {
                            if (interval[4].length > 0 && intervalSlide[3] > 0) {
                                if (isInTimespan(intervalSlide)) {
                                    intervalSlides.push(intervalSlide);
                                }
                            } else {
                                intervalSlides.push(intervalSlide);
                            }
                        }
                    }
                    break;
            }
        }
    }, 1000);

}

/**
 *  Calulate the timespan and check if the slide in the given timespan.
 *  @returns {boolean} returns true if the image in given timespan
 */
function isInTimespan(values) {
    for (let i = 4; i < values.length; i++) {
        if (values[4].length > 0) {
            let strPeriodOfTime = values[3];
            let objCurrentDate = new Date();
            let strTime = values[i];
            let objDate = new Date();
            if (strTime.length > 0 && strPeriodOfTime.length > 0) {
                const intPeriodOfTime = parseInt(strPeriodOfTime.substr(0, strPeriodOfTime.length - 1));
                let intHours = parseInt(strTime.split(":")[0]);
                let intMinutes = parseInt(strTime.split(":")[1]);
                let charTimeUnit = strPeriodOfTime.charAt(strPeriodOfTime.length - 1).toLowerCase();
                objDate.setHours(intHours);
                objDate.setMinutes(intMinutes);
                objCurrentDate.setTime(objCurrentDate.getTime() + (timeOffset) * 60 * 60 * 1000);
                switch (charTimeUnit) {
                    case 'm':
                        if ((objCurrentDate.getTime() - objDate.getTime()) / 60000 >= 0) {
                            if ((objCurrentDate.getTime() - objDate.getTime()) / 60000 <= intPeriodOfTime) {
                                return true;
                            }
                        }
                        break;
                    case 'h':
                        if ((objCurrentDate.getTime() - objDate.getTime()) / 36E+6 >= 0) {
                            if ((objCurrentDate.getTime() - objDate.getTime()) / 36E+6 <= intPeriodOfTime) {
                                return true;
                            }
                        }
                        break;
                }
            }
        }
    }
    return false;
}


/**
 * @param strTime
 * @returns {number} returns the time in milliseconds
 */
function convertToMilliseconds(strTime) {
    let charTimeUnit = strTime.charAt(strTime.length - 1);
    let intTime = strTime.substr(0, strTime.length - 1);
    let milliseconds;
    switch (charTimeUnit) {
        case 's':
            milliseconds = intTime * 1000;
            break;
        case 'm':
            milliseconds = intTime * 60000;
            break;
        case 'h':
            milliseconds = intTime * 3.6E+6;
            break;
    }
    return milliseconds;
}

/**
 * @param charTimeUnit
 * @param intTime
 * @returns {number} returns the time in milliseconds
 */
function convertTimeUnitToMilliseconds(charTimeUnit, intTime) {
    let milliseconds;
    switch (charTimeUnit) {
        case 's':
            milliseconds = intTime * 1000;
            break;
        case 'm':
            milliseconds = intTime * 60000;
            break;
        case 'h':
            milliseconds = intTime * 3.6E+6;
            break;
    }
    return milliseconds;
}

/**
 * Updates the stream displayDurationDelay
 * and calculate the time unit in milliseconds.
 * If the stream has the value "infinity" only the livestream will be displayed
 */
function updateStreamDelay() {
    for (let i = 0; i < config.length; i++) {
        let imageName = config[i][0];
        if (config[i][1] != null) {
            let duration = config[i][1];
            const timeUnit = duration[duration.length - 1];
            const time = parseInt(duration.substr(0, duration.length - 1));
            //Get the name of the file
            if (isLivestream(imageName)) {
                if (duration.toLowerCase() === "infinity") {
                    onlyStreaming = true;
                } else if (slideIndex >= imageArray.length) {
                    displayDurationDelay = convertTimeUnitToMilliseconds(timeUnit, time);
                    onlyStreaming = false;
                }

            }
        }
    }
}

/**
 * If the stream is muted it wont be displayed
 * @param state set muted or not
 */
function slideShowState(state) {
    switchAnimation = !switchAnimation;
    switch (state) {
        case 0:
            enableId("imageSlideShow");
            enableId("imageSlideShowAnimation");
            if (displayGroupSlides.length > 1) {
                fadeOutIfSlideDisabled("livestream");
                fadeOutIfSlideDisabled("videoSlideShow");
                fadeOutIfSlideDisabled("videoSlideShowAnimation");
                if (switchAnimation) {
                    fadeIn("imageSlideShowAnimation");
                    fadeOut("imageSlideShow");
                } else {
                    fadeIn("imageSlideShow");
                    fadeOut("imageSlideShowAnimation");
                }
            }
            break;
        case 1:
            enableId("videoSlideShow");
            enableId("videoSlideShowAnimation");
            if (displayGroupSlides.length > 1) {
                fadeOutIfSlideDisabled("imageSlideShow");
                fadeOutIfSlideDisabled("imageSlideShowAnimation");
                fadeOutIfSlideDisabled("livestream");
                if (switchAnimation) {
                    fadeIn("videoSlideShowAnimation");
                    fadeOut("videoSlideShow");
                } else {
                    fadeIn("videoSlideShow");
                    fadeOut("videoSlideShowAnimation");
                }
            }
            break;
        case 2:
            enableId("livestream");
            if (displayGroupSlides.length > 1) {
                fadeOutIfSlideDisabled("imageSlideShow");
                fadeOutIfSlideDisabled("videoSlideShow");
                fadeOutIfSlideDisabled("imageSlideShowAnimation");
                fadeOutIfSlideDisabled("videoSlideShowAnimation");
                fadeIn("livestream");
                document.getElementById("livestream").volume = 1;
                document.getElementById("livestream").muted = false;
            }
            break;

    }
}

function disableSlideAfterAnimationEnded(elementId) {
    let fileName = document.getElementById(elementId).src.split("/");
    fileName = decodeURI(fileName[fileName.length - 1]);
    for (let displayGroupSlide of displayGroupSlides) {
        if (fileName === displayGroupSlide.name || elementId === displayGroupSlide.name) {
            document.getElementById(elementId).addEventListener("animationend", function () {
                if (document.getElementById(elementId).className === "animated " + displayGroupSlide.fadeOut) {
                    disableId(elementId);
                }
            });
        }
    }
}

function fadeIn(elementId) {
    let fileName = document.getElementById(elementId).src.split("/");
    fileName = decodeURI(fileName[fileName.length - 1]);
    for (let displayGroupSlide of displayGroupSlides) {
        if (fileName === displayGroupSlide.name || elementId === displayGroupSlide.name) {
            $("#" + elementId).removeClass().addClass("animated " + displayGroupSlide.fadeIn);
        }
    }
}

function fadeOut(elementId) {
    let fileName = document.getElementById(elementId).src.split("/");
    fileName = decodeURI(fileName[fileName.length - 1]);
    for (let displayGroupSlide of displayGroupSlides) {
        if (fileName === displayGroupSlide.name || elementId === displayGroupSlide.name) {
            $("#" + elementId).removeClass().addClass("animated " + displayGroupSlide.fadeOut);
        }
    }
}

function fadeOutIfSlideDisabled(elementId) {
    let fileName = document.getElementById(elementId).src.split("/");
    fileName = decodeURI(fileName[fileName.length - 1]);
    for (let displayGroupSlide of displayGroupSlides) {
        if (fileName === displayGroupSlide.name || elementId === displayGroupSlide.name) {
            if ($("#" + elementId).attr('class') === "animated " + displayGroupSlide.fadeIn) {
                $("#" + elementId).removeClass().addClass("animated " + displayGroupSlide.fadeOut);
            }
        }
    }
    disableSlideAfterAnimationEnded(elementId);
}

/**
 * Set of the given ID display none
 * @param id
 */
function disableId(id) {
    const objDiv = document.getElementById(id);
    if (objDiv)
        objDiv.style.display = "none";
    if (id === "livestream") {
        document.getElementById("livestream").muted = true;
    }
}

/**
 * Set of the given ID display block
 * @param id
 */
function enableId(id) {
    const objDiv = document.getElementById(id);
    if (objDiv)
        objDiv.style.display = "block";
}
