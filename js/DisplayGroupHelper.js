function saveDisplayGroup(displayName, localIP, slides) {
    let xhttp = new XMLHttpRequest();
    xhttp.open("POST", "../gui/utils/AddDisplayGroupData.php", true);
    xhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhttp.send("jsonString=" + JSON.stringify(getJsonObject(displayName, localIP, slides)) + "&fileName=" + localIP);
}

function deleteDisplayGroup(localIP) {
    let xhttp = new XMLHttpRequest();
    xhttp.open("POST", "../gui/utils/DeleteDisplayGroupData.php", true);
    xhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhttp.send("fileName=" + localIP);
    console.log(localIP);
}

function getJsonObject(displayName, localIPAddress, slides) {
    return {
        displayName: displayName,
        localIPAddress: localIPAddress,
        slides: slides
    };
}
