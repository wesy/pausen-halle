/**
 * Created by PhpStorm.
 * User: Sebastian Kotte
 * Date: 11.01.2019
 * Time: 18:28
 */

/**
 * @param filePath
 */
function readConfigFile(filePath) {
    let rawFile = new XMLHttpRequest();
    let allText = "";
    rawFile.open("GET", filePath, false);
    rawFile.onreadystatechange = function () {
        if (rawFile.readyState === 4) {
            if (rawFile.status === 200 || rawFile.status === 0) {
                allText = rawFile.responseText;
            }
        }
    };
    rawFile.send(null);
    return allText;
}