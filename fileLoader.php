<?php
/**
 * Created by PhpStorm.
 * User: Sebastian Kotte
 * Date: 08.12.2018
 * Time: 20:00
 */

$path = "slideshow/";
$imgfiles = glob($path . '*.{jpg,jpeg,png,gif,svg,mp4,avi,flv,mov,WebM}', GLOB_BRACE);
sort($imgfiles, SORT_NATURAL | SORT_FLAG_CASE);
echo "var imageArray = " . json_encode($imgfiles) . ";\r\n";